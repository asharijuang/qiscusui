//
//  QiscusManager.swift
//  QChat
//
//  Created by asharijuang on 8/24/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import Foundation
import Qiscus

class QiscusManager {
    
    private static let instance = QiscusManager()
    
    public static var sharedInstance:QiscusManager {
        get {
            return instance
        }
    }
    public static var bubbles : [String]    = []
    var chatService = QChatService()
    
    func login(appID id: String, email: String, username: String, key: String) {
        Qiscus.setup( withAppId: id,
                      userEmail: email,
                      userKey: key,
                      username: username,
                      avatarURL: "",
                      delegate: self
        )
        QiscusCommentClient.sharedInstance.roomDelegate = self
        Qiscus.registerNotification()
    }
    
    func isLogin() -> Bool {
        return Qiscus.isLoggedIn
    }
    
    func clear() {
        Qiscus.clear()
    }
    
    // MARK: Action and View
    public func chatView(forId id: Int?, consultationID: String?, userName: String, thumbURL: String, status : String, action : [ChatAction]? = nil, completionHandler: @escaping (UIViewController, NSError) -> Void) {
        if id == nil && consultationID == nil {
            return
        }
        let chatVC              = QChatView()
        chatVC.actions          = action
        chatVC.chatRoomUniqueId = consultationID
        chatVC.chatRoomId       = id
//        chatVC.chatAvatarURL = thumbURL
        chatVC.chatTitle        = "Halodoc"
        chatVC.customCells      = self.initCustomCell()
        completionHandler(chatVC, NSError())
    }
    
    func sendMessage(withID id: String, type: MessageType = .text, message: String, completion: @escaping (Bool) -> Void) {
        // Folow chat manager
    }
    
    func getRoomInfo(uniqueId id: String, completion: @escaping (QRoom?, Bool) -> Void) {
        QChatService().room(withUniqueId: id, title: "", avatarURL: "", onSuccess: { (room) in
            completion(room, false)
        }) { (error) in
            print(error)
            completion(nil, true)
        }
    }
    // MARK: Notification
    
    /*
     func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    */
    func registerDeviceToken(token: Data) {
        Qiscus.didRegisterUserNotification(withToken: token)
    }
    
    /*
     func application(_ application: UIApplication, didReceive notification: UILocalNotification)
    */
    func didReceive(notification: UILocalNotification) {
        Qiscus.didReceiveNotification(notification: notification)
    }
    
    /*
     func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])
    */
    func didReceive(RemoteNotification userInfo: [AnyHashable : Any]) {
        Qiscus.didReceive(RemoteNotification: userInfo)
    }
    
    /*
     Register custom bubles for Chat, refer with enum MessageType
     
    */
    private func initCustomCell() -> [QChatCell]{
        var cells : [QChatCell] = [QChatCell]()
        // default type cell
        let cell0 = UndefinedCell()
        cell0.initialize(name: "Undefined")
        cell0.height    = 0.0
        cells.append(cell0)

        let cell1 = PatientInfoCell()
        cell1.initialize(name: "PatientInfo")
        cell1.height    = 173.0
        cells.append(cell1)

        let cell2 = DoctorNoteCell()
        cell2.initialize(name: "DoctorNote")
        cell2.height    = 170.0
        cells.append(cell2)

        let cell3 = MessageWelcomeCell()
        cell3.initialize(name: "MessageWelcome")
        cell3.height    = 50
        cells.append(cell3)

        let cell4 = CallStatusCell()
        cell4.initialize(name: "CallStatus")
        cell4.height    = 50
        cells.append(cell4)

        let cell5 = AttachmentCell()
        cell5.initialize(name: "Attachment")
        cell5.height    = 200.0
        cells.append(cell5)

        let cell8 = MessageTextCell()
        cell8.initialize(name: "MessageText")
        cells.append(cell8)
        
        return cells
    }
}

extension QiscusManager : QiscusConfigDelegate {
    func qiscusFailToConnect(_ withMessage:String){
        print(withMessage)
    }
    
    func qiscusConnected(){
//        self.goToChatNavigationView()
    }
    
    func qiscus(gotSilentNotification comment: QComment) {
        Qiscus.createLocalNotification(forComment: comment)
    }
    
    func qiscus(didTapLocalNotification comment: QComment, userInfo: [AnyHashable : Any]?) {
        QChat.sharedInstance.getConversationVC(forId: comment.roomId, userName: "patient", thumbURL: "", status: "", completionHandler: { (target, error) in
            self.currentViewController()?.hidesBottomBarWhenPushed = true
            self.currentViewController()?.navigationController?.pushViewController(target, animated: true)
        })
    }
    
    func currentViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return currentViewController(base: nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController
            
            if let top = moreNavigationController.topViewController, top.view.window != nil {
                return currentViewController(base: top)
            } else if let selected = tab.selectedViewController {
                return currentViewController(base: selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return currentViewController(base: presented)
        }
        
        return base
    }
}

extension QiscusManager : QiscusRoomDelegate {
    func gotNewComment(_ comments:QComment) {
        
    }
    func didFinishLoadRoom(onRoom room: QRoom) {
        
    }
    func didFailLoadRoom(withError error:String) {
        
    }
    func didFinishUpdateRoom(onRoom room:QRoom) {
        
    }
    func didFailUpdateRoom(withError error:String) {
        
    }
}
