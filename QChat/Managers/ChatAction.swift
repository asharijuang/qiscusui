//
//  QChatAction.swift
//  QChat
//
//  Created by asharijuang on 7/26/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import Foundation

public class ChatAction: NSObject {
    var handler : ((ChatAction) -> Void)? = nil
    
    public init(title: String?, handler: ((ChatAction) -> Void)? = nil) {
        super.init()
        if handler != nil {
            self.handler = handler!
        }
    }
}
