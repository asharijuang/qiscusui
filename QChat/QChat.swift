//
//  QChat.swift
//  QChat
//
//  Created by asharijuang on 7/17/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import UIKit
import Qiscus

/*
 Custom message MIME Type
 */
public enum MessageType : String {
    case text
    case image
    case video
    case audio
    case callAudio
    case callVideo
    // Dummy
    case note
    case patientInfo
    case callStatus
    case welcome
}


public class QChat: NSObject {

    private static let instance = QChat()
    
    public static var sharedInstance:QChat {
        get {
            return instance
        }
    }
    
    var chatManager         = QiscusManager.sharedInstance
    class var bundle:Bundle{
        get{
            let podBundle   = Bundle(for: QChat.self)
            
            if let bundleURL = podBundle.url(forResource: "QChat", withExtension: "bundle") {
                return Bundle(url: bundleURL)!
            }else{
                return podBundle
            }
        }
    }
    
    public func setup(appID id: String, email: String, username name: String, key: String) {
        chatManager.login(appID: id, email: email, username: name, key: key)
    }
    
    public func isLogin() -> Bool {
        return QiscusManager.sharedInstance.isLogin()
    }
    
    public func clear() {
        chatManager.clear()
    }
    
    public func getConversationVC(forId id: Int? = nil, consultationID: String? = nil, userName: String, thumbURL: String, status : String, action: [ChatAction]? = nil, completionHandler: @escaping (UIViewController, NSError) -> Void) {
        self.chatManager.chatView(forId: id, consultationID: consultationID, userName: userName, thumbURL: thumbURL, status: status, action: action) { (target, error) in
            completionHandler(target, error)
        }
    }

    public func sendMessage(withID id: String, type: MessageType = .text, message: String, completion: @escaping (Bool) -> Void) {
        chatManager.sendMessage(withID: id, type: type, message: message) { (success) in
            if success {
                completion(true)
            }else {
                completion(false)
            }
        }
    }
    
    public func getRoominfo(id: String, completion: @escaping (QRoom?, Bool) -> Void) {
        chatManager.getRoomInfo(uniqueId: id) { (room, error) in
            completion(room, error)
        }
    }
    
    // Notification
    /*
     func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
     */
    public func registerDeviceToken(token: Data) {
        chatManager.registerDeviceToken(token: token)
    }
    
    /*
     func application(_ application: UIApplication, didReceive notification: UILocalNotification)
     */
    public func didReceive(notification: UILocalNotification) {
        chatManager.didReceive(notification: notification)
    }
    
    /*
     func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])
     */
    public func didReceive(RemoteNotification userInfo: [AnyHashable : Any]) {
        chatManager.didReceive(RemoteNotification: userInfo)
    }
}
