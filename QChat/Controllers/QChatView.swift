//
//  QChatView.swift
//  QChat
//
//  Created by asharijuang on 8/23/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import UIKit
import Qiscus

class QChatView: QiscusChatVC {
    
    var actions : [ChatAction]?     = nil
    var customCells : [QChatCell]?  = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // self.collectionViewTopMargin.constant = 100
        self.backgroundView.isHidden    = true

        let iconCall        = UIImage(named: "ic_phone_call", in: QChat.bundle, compatibleWith: nil)
        let iconCallVideo   = UIImage(named: "ic_video_call", in: QChat.bundle, compatibleWith: nil)
        let iconEnd         = UIImage(named: "ic_end_consultation", in: QChat.bundle, compatibleWith: nil)
        
        let endButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
        endButton.setBackgroundImage(iconEnd, for: .normal)
        endButton.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        endButton.addTarget(self, action: #selector(endConsultation), for: .touchUpInside)
        let barButtonEnd    = UIBarButtonItem(customView: endButton)

        let callButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        callButton.setBackgroundImage(iconCall, for: .normal)
        callButton.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        callButton.addTarget(self, action: #selector(callAudio), for: .touchUpInside)
        let barButtonCall    = UIBarButtonItem(customView: callButton)
        
        let callVideoButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
        callVideoButton.setBackgroundImage(iconCallVideo, for: .normal)
        callVideoButton.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        callVideoButton.addTarget(self, action: #selector(callVideo), for: .touchUpInside)
        let barButtonCallVideo    = UIBarButtonItem(customView: callVideoButton)
        
        navigationItem.rightBarButtonItems = [barButtonEnd, barButtonCallVideo, barButtonCall]
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.registerCustomCell()
    }
    
    func registerCustomCell() {
        if self.customCells != nil {
            for cell in self.customCells! {
                self.collectionView.register(UINib(nibName: cell.nibName,bundle: QChat.bundle), forCellWithReuseIdentifier: cell.identifier)
            }
        }
    }
    
    func callAudio() {
        postComment(type: "audioCall", payload: "{}", text: "Audio Call at 00")
    }
    
    func callVideo() {
        postComment(type: "DoctorNote", payload: "{}", text: "Video Call at 00")
    }
    
    func endConsultation() {
        postComment(type: "endConsultation", payload: "{}", text: "Semoga Lekas sembuh")
    }
    
    func postComment(type: String, payload: String, text: String) {
        let newComment = self.chatRoom?.newCustomComment(type: type, payload: payload, text: text)
        self.chatRoom?.post(comment: newComment!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let comment = self.chatRoom!.comment(onIndexPath: indexPath)!
//        let group = self.chatRoom!.comments[indexPath.section]
        
            var cells = QChatCell()
            for cell in self.customCells! {
                print("type: \(comment.typeRaw) / \(self.customCells?.count)")
                print("identifier: \(cell.identifier)")
                if cell.identifier == comment.typeRaw {
                    cells = collectionView.dequeueReusableCell(withReuseIdentifier: cell.identifier, for: indexPath) as! QChatCell
                    //cell.setupCell(content: chat)
                    self.collectionView.reloadItems(at: [indexPath])
                }else {
                    return super.collectionView(collectionView, cellForItemAt: indexPath)
                }
            }
            return cells
            
    }
    
    // MARK: CollectionView delegateFlowLayout
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let cellSquareSize: CGFloat = screenWidth
        
        
        let comment = self.chatRoom!.comment(onIndexPath: indexPath)!
//        let group = self.chatRoom!.comments[indexPath.section]
        
        if comment.typeRaw == "DoctorNote" {
            return CGSize(width: cellSquareSize, height: 150)
            //return CGSize(width:cellSquareSize, height:(bubble?.height)!)
        }else {
            return super.collectionView(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 0.0, 0.0)
    }

}
