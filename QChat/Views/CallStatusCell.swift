//
//  QMessageCallStatusCell.swift
//  QChat
//
//  Created by Qiscus Pte Ltd on 17/7/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import UIKit

class CallStatusCell: QChatCell {

    @IBOutlet var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.height = 50.0
    }

//    override func setupCell(content: QMessage) {
//        super.setupCell(content: content)
//        self.nameLabel.text = "Calling"
//    }
}
