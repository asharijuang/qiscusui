//
//  QMessageDRMRCell.swift
//  QChat
//
//  Created by Qiscus Pte Ltd on 17/7/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import UIKit

class DoctorNoteCell: QChatCell {

    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var messageLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialize()
        self.height = 273.0
    }

    func initialize() {
    }
}
