//
//  QOutgoingMessageAttachmentViewCell.swift
//  QChat
//
//  Created by Qiscus Pte Ltd on 17/7/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import UIKit
import Qiscus

class AttachmentCell: QChatCell {

    @IBOutlet var bubbleImageView: UIImageView!
    
    @IBOutlet var dataLabel: UILabel!
    @IBOutlet var statusImageView: UIImageView!
    @IBOutlet var statusContainerView: UIView!
    @IBOutlet var attachmentImageView: UIImageView!
    
    @IBOutlet var attactmentView: QRoundRectView!
    override func awakeFromNib() {
        super.awakeFromNib()
        attactmentView.layer.cornerRadius = 6
        attactmentView.layer.borderWidth = 0.1
        attactmentView.layer.borderColor = UIColor.lightGray.cgColor
        
        bubbleImageView.layer.cornerRadius = 6
        bubbleImageView.layer.borderWidth = 0.1
        bubbleImageView.layer.borderColor = UIColor.lightGray.cgColor
        
    }

    override func setupCell(content: QComment) {
        super.setupCell(content: content)
        self.dataLabel.text = self.content?.time
        //self.attachmentImageView.image  = content.image
//        self.bubbleImageView.af_setImage(withURL: URL(string:content.imageUrl!)!, placeholderImage: content.image)
        
        
        self.dataLabel.text = self.content?.time

        self.attactmentView.backgroundColor  = UIColor.init(red: 255/255, green: 205/255, blue: 222/255, alpha: 1)

        
        self.statusImageView.image  = UIImage(named: "ic_status_delivered", in: QChat.bundle, compatibleWith: nil)
        
        // status
        //        switch content.status {
        //        case .sent:
        //            self.statusImageView.image  = UIImage(named: "ic_status_sent", in: QChat.bundle, compatibleWith: nil)
        //        case .pending:
        //            self.statusImageView.image  = UIImage(named: "ic_status_pending", in: QChat.bundle, compatibleWith: nil)
        //        case .failed:
        //            self.statusImageView.image  = UIImage(named: "ic_status_failed", in: QChat.bundle, compatibleWith: nil)
        //        case .delivered:
        //            self.statusImageView.image  = UIImage(named: "ic_status_delivered", in: QChat.bundle, compatibleWith: nil)
        //        default:
        //            break
        //        }
        
        
    }
}



