//
//  QMessagePatientInfoCell.swift
//  QChat
//
//  Created by Qiscus Pte Ltd on 17/7/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import UIKit

class PatientInfoCell: QChatCell {

    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var weightUnitLabel: UILabel!
    @IBOutlet var weightLabel: UILabel!
    @IBOutlet var heightUnitLabel: UILabel!
    @IBOutlet var heightLabel: UILabel!
    @IBOutlet var ageUnitLabel: UILabel!
    @IBOutlet var ageLabel: UILabel!
    @IBOutlet var infoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.height = 273.0
    }

}
