//
//  QMessageWelcomeMessageCell.swift
//  QChat
//
//  Created by Qiscus Pte Ltd on 17/7/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import UIKit
import Qiscus

class MessageWelcomeCell: QChatCell {

    @IBOutlet var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setupCell(content: QComment) {
        super.setupCell(content: content)
        self.nameLabel.text = "Notification Call"
    }

}
