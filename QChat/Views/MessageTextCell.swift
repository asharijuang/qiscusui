//
//  QOutgoingMessageTextViewCell.swift
//  QChat
//
//  Created by Qiscus Pte Ltd on 17/7/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import UIKit
import Qiscus

class MessageTextCell: QChatCell {
    @IBOutlet var statusImageView       : UIImageView!
    @IBOutlet var bubleView             : UIView!
    @IBOutlet var widthConstant         : NSLayoutConstraint!
    @IBOutlet var dateLabel             : UILabel!
    @IBOutlet var statusContainerView   : UIView!
    @IBOutlet var messageLabel          : UILabel!
    @IBOutlet var imageView             : UIImageView!
    
    @IBOutlet weak var leftConsstraint: NSLayoutConstraint!
    @IBOutlet weak var rightConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bubleView.layer.cornerRadius = 6
        bubleView.layer.borderWidth = 0.1
        bubleView.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func setupCell(content: QComment) {
        super.setupCell(content: content)
        self.dateLabel.text = content.time
        self.messageLabel.text  = content.text
//        if !(self.content?.isOwnMessage)! {
//            self.bubleView.backgroundColor  = UIColor.white
//            self.leftConsstraint.constant   = 10.0
//            self.rightConstraint.constant   = 70.0
//        }else {
//            self.bubleView.backgroundColor  = UIColor.init(red: 255/255, green: 205/255, blue: 222/255, alpha: 1)
//            self.leftConsstraint.constant   = 70.0
//            self.rightConstraint.constant   = 10.0
//        }
        
        self.statusImageView.image  = UIImage(named: "ic_status_delivered", in: QChat.bundle, compatibleWith: nil)
        
        // status
//        switch content.status {
//        case .sent:
//            self.statusImageView.image  = UIImage(named: "ic_status_sent", in: QChat.bundle, compatibleWith: nil)
//        case .pending:
//            self.statusImageView.image  = UIImage(named: "ic_status_pending", in: QChat.bundle, compatibleWith: nil)
//        case .failed:
//            self.statusImageView.image  = UIImage(named: "ic_status_failed", in: QChat.bundle, compatibleWith: nil)
//        case .delivered:
//            self.statusImageView.image  = UIImage(named: "ic_status_delivered", in: QChat.bundle, compatibleWith: nil)
//        default:
//            break
//        }
    }
}
