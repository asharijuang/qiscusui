//
//  QChatCell.swift
//  QChat
//
//  Created by asharijuang on 7/17/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import UIKit
import Qiscus

class QChatCell: UICollectionViewCell {
    
    var nibName     : String    = ""
    var identifier  : String    = ""
    var height      : CGFloat   = 0.0
    var content     : QComment?
    
    /**
     Register Custom cell and set identifier and cell identifier
     
     - parameter name: Name of cell identifier
     */
    func initialize(name: String) {
        self.nibName            = name + "Cell"
        self.identifier         = name
    }
    
    /**
     Fill Content of cell, and render in every custom cell
     
     - parameter content: QMessage
     */
    func setupCell(content: QComment){
        // implementation will be overrided on child class
        self.content = content
    }
}
