//
//  ViewController.swift
//  example
//
//  Created by asharijuang on 7/17/17.
//  Copyright © 2017 qiscus. All rights reserved.
//

import UIKit
import QChat

class ViewController: UIViewController {

    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var roomText: UITextField!
    @IBOutlet weak var roomsText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Sample Chat"
    }
    
    override func viewWillAppear(_ animated: Bool) {
         navigationController?.navigationBar.barTintColor = UIColor.brown
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var clickChatView: UIButton!
    
    @IBAction func clickChat(_ sender: Any) {
        QChat.sharedInstance.getConversationVC(consultationID: roomText.text!, userName: "qiscus", thumbURL: "", status: "") { (target, error) in
            self.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(target, animated: true)
        }
        
    }
    @IBAction func clickLogin(_ sender: Any) {
        QChat.sharedInstance.setup(appID: "halodoc-stag2", email: self.emailText.text!, username: "patient", key: "password")
    }
    
    @IBAction func goChat(_ sender: Any) {
        var actionVC    : [ChatAction]  = [ChatAction]()
        var calltype    : String        = "video"
        let user            = "halodoc"
        let timeout         = 1000
        let requestTimeOut  = 1000
        let time            = NSDate().timeIntervalSince1970 * 1000
        let roomID          = self.roomText.text!
        // action 1 audio call
        let audioCall   = ChatAction(title: "audio call", handler: { (action) in
            //
            // send call message
            calltype = "audio"
            let callSignal : String = "{\r\n \"consultation_id\" : \"\(roomID)\",\r\n  \"conversation_id\" : \"\(roomID)\",\r\n  \"av_room_id\" : \"\(roomID)\",\r\n  \"call_init_user\": \"\(user)\",\r\n  \"call_type\" : \"\(calltype)\",\r\n  \"request_time\" : \(time),\r\n  \"request_timeout\" : \(timeout),\r\n  \"call_time_out\" : \(requestTimeOut)\r\n}"
            QChat.sharedInstance.sendMessage(withID: self.roomText.text!, type: .callAudio, message: callSignal, completion: { success in
                print("send message \(success)")
            })
        })
        actionVC.append(audioCall)
        // action 2 video call
        let videoCall   = ChatAction(title: "video call", handler: { (action) in
            //
            print("video call")
            // send call message
            calltype = "video"
            let callSignal : String = "{\r\n \"consultation_id\" : \"\(roomID)\",\r\n  \"conversation_id\" : \"\(roomID)\",\r\n  \"av_room_id\" : \"\(roomID)\",\r\n  \"call_init_user\": \"\(user)\",\r\n  \"call_type\" : \"\(calltype)\",\r\n  \"request_time\" : \(time),\r\n  \"request_timeout\" : \(timeout),\r\n  \"call_time_out\" : \(requestTimeOut)\r\n}"
            QChat.sharedInstance.sendMessage(withID: self.roomText.text!, type: .callAudio, message: callSignal, completion: { success in
                print("send message \(success)")
            })
        })
        actionVC.append(videoCall)
        // action 3 end Consultation
        let endConsultation   = ChatAction(title: "end consultation", handler: { (action) in
            //
            print("end consultation")
        })
        actionVC.append(endConsultation)
        QChat.sharedInstance.getConversationVC(consultationID: roomText.text!, userName: "qiscus", thumbURL: "", status: "", action: actionVC) { (vc, error) in
            
            self.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func listChat(_ sender: Any) {

    }
    
}

