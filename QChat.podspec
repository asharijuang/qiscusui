Pod::Spec.new do |s|

s.name         = "QChat"
s.version      = "1.7.5"
s.summary      = "Halodoc Chat module with Qiscus SDK IOS."

s.homepage     = "http://qiscus.com"
# s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"

s.license      = "MIT"

s.author       = { "juang@qiscus.co" => "juang@qiscus.co" }

s.platform     = :ios, "9.0"

#s.source       = { :path => "." }
s.source       = { :git => 'https://gitlab.devops.mhealth.tech/ariefnurputranto/QChat', :tag => s.version.to_s }

s.source_files  = "QChat", "QChat/**/*.{h,m,swift,xib}"

s.resources = "QChat/**/*.xcassets"
s.resource_bundles = {
    'QChat' => ['QChat/**/*.{lproj,xib,xcassets,imageset,png,mp3}']
}

s.framework		= 'UIKit', 'AVFoundation'
s.requires_arc	= false

s.dependency "Qiscus"
s.dependency "Alamofire"
s.dependency "AlamofireImage"
s.dependency "SwiftyJSON"
# s.dependency "Nias"

end
